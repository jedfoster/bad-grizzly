var express = require('express');
var app = module.exports = express();
var nodeSass = require('node-sass');
var bodyParser = require('body-parser');

app.use(bodyParser.json());

app.set('view engine', 'jade');

app.set('views', __dirname + '/views');

app.use(express.static(__dirname + '/../public'));

var index = function(req, res) {
  res.render('index', {
    pageTitle: 'Hello, ' + process.env.USER,
    user: process.env.USER
  });
};

app.get('/', index);
app.get('/gist/*', index);

var compile = function(req, res) {
  var stats = {};

  var css = nodeSass.render({
    data: req.body.compiler.sass + ' ',
    outputStyle: req.body.compiler.outputStyle,
    stats: stats,

    success: function(css) {
      res.json({
        compiler: {
          css: css,
          stats: stats
        }
      });
    },

    error: function(error) {
      res.status(500).send(error);
    }
  });
};

app.post('/compile', compile);
app.put('/compile', compile);

// With the express server and routes defined, we can start to listen
// for requests. Heroku defines the port in an environment variable.
// Our app should use that if defined, otherwise 3000 is a pretty good default.
app.port = process.env.PORT || 3000;

app.listen(app.port);
console.log("The server is now listening on port %s", app.port);


var request = require('supertest');
var app = require('../app/index.js');

describe("POST /compile", function() {
  it("responds successfully", function(done) {
    request(app)
      .post('/compile')
      .send({
        compiler: {
          sass: '$red: #f00;\n.test {\n  color: $red;\n}',
          outputStyle: 'compressed'
        }
      })
      .set('Content-Type', 'application/json')
      .expect(function(res) {
        if(res.body.compiler.css != '.test{color:#f00;}') throw new Error('expected ".test{color:#f00;}", got "' + res.body.compiler.css + '"');
      })
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});


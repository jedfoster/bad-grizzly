bad-grizzly
===========

I was asked by a friend to give a guest lecture on Node.js and Ember to a Code Fellows class last year. This is the project we built during that lecture.

The app is a simple clone of my site, [SassMeister.com](http://sassmeister.com); basically, you type Sass in the input field and the app shows the compiled CSS in another field. The functionality is trivial and the UI is barebones, but it shows some of the neat things Node.js, Express, and Ember can do. The server side includes a JSON API—with tests!—and the client-side shows off Ember features such as data binding, routes, and models. And all that in under 200 lines of code, including tests for the API.

You can see the app in action here: https://bad-grizzly.herokuapp.com

And you can read a very long article I wrote as the basis of the lecture here: https://github.com/jedfoster/bad-grizzly/tree/article


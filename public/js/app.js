var App = Ember.Application.create();

App.Router.map(function() {
  this.resource('gist', { path: 'gist/:gist_id' });
});

App.Router.reopen({
  location: 'history'
});

App.ApplicationAdapter = DS.RESTAdapter.extend({
  buildURL: function(type, id, record) {
    if(type == 'gist') return 'http://gist.drft.io/gists/' + id + '.json';

    return '/compile';
  }
});

var schema = {
  sass: DS.attr('string'),
  css: DS.attr('string'),
  outputStyle: DS.attr('string'),
  stats: DS.attr()
};

App.Compiler = DS.Model.extend(schema);

App.Gist = DS.Model.extend(schema);

App.IndexRoute = Ember.Route.extend({
  model: function () {
    return this.store.createRecord('compiler');
  }
});

App.IndexController = Ember.ObjectController.extend({
  outputStyles: ['nested', 'compressed'],

  selectedOutputStyle: 'nested',

  compileOnEntry: function() {
    this.send('compile');
  }.observes('sassInput', 'selectedOutputStyle'),

  actions: {
    compile: function() {
      var self = this;

      this.set('sass', this.get('sassInput'));
      this.set('outputStyle', this.selectedOutputStyle);

      if(! Ember.isBlank(this.get('sassInput'))) {
        this.get('model').save()
          .then(function(response) {
            console.log('Success: ', response);
          },
          function(err) {
            console.log('Error: ', err);
            self.set('css', err.responseText);
          });
      }
    }
  }
});

